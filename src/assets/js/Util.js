const QRCode = require('qrcode')

class Util {

    static IdentityCodeValid(code) {
          var city={11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江 ",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北 ",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏 ",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外 "};
          var tip = "";
          var pass= true;

          if(!code || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(code)){
            tip = "身份证号格式错误";
            pass = false;
          }

          else if(!city[code.substr(0,2)]){
            tip = "地址编码错误";
            pass = false;
          }
          else{
            //18位身份证需要验证最后一位校验位
            if(code.length == 18){
              code = code.split('');
              //∑(ai×Wi)(mod 11)
              //加权因子
              var factor = [ 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 ];
              //校验位
              var parity = [ 1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2 ];
              var sum = 0;
              var ai = 0;
              var wi = 0;
              for (var i = 0; i < 17; i++)
              {
                ai = code[i];
                wi = factor[i];
                sum += ai * wi;
              }
              var last = parity[sum % 11];
              if(parity[sum % 11] != code[17]){
                tip = "校验位错误";
                pass =false;
              }
            }
          }
          //if(!pass) alert(tip);
          return pass;
        } 
    /**
     * 判断给定字符是否是中文
     * @param temp
     * @returns {Boolean}
     */
    static isChinese(s) {
        var patrn = /[\u4E00-\u9FA5]|[\uFE30-\uFFA0]/gi
        var flag = true
        if (!patrn.exec(s)) {
            flag = false
        }
        return flag
    }

    /**
     * 验证给定字符是否是A-Za-z之间的英文字母
     */
    static isEnglish(value) {
        var str = /^[A-Za-z]*$/
        var flag = false
        if (str.test(value)) {
            flag = true
        }
        return flag
    }

    static isValidNum(value) {

        var str = /^(-|\+)?\d+$/
        var flag = false
        if (str.test(value)) {
            flag = true
        }
        return flag


    }


    /**
     * 是否全是数字
     * @param p
     * @returns
     */
    static isAllNum(p) {
        var re = /^\d*$/
        return re.test(p)
    }

    /**
     * 验证手机号
     */
    static isValidPhone(p) {
        var re = /^1\d{10}$/
        return re.test(p)
    }

    /**
     * 验证身份证号
     */
    static isValidID(p) {
        p = String(p).toUpperCase()
        var re = /(^\d{15}$)|(^\d{17}([0-9]|X$))/
        return re.test(p)
    }

    /**
     * 验证邮箱地址合法性
     * @param temp
     * @returns {Boolean}
     */
    static isValidEmail(s) {
        var re = /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/;
        return re.test(s);
    }

    /**
     * 下载图片
     * @param content 图片的base64编码
     * @param filename
     */
    static downloadImgByBase64(content, fileName) {
        fileName += '.png';
        let base64ToBlob = function (code) {
            let parts = code.split(';base64,');
            let contentType = parts[0].split(':')[1];
            let raw = window.atob(parts[1]);
            let rawLength = raw.length;
            let uInt8Array = new Uint8Array(rawLength);
            for (let i = 0; i < rawLength; ++i) {
                uInt8Array[i] = raw.charCodeAt(i);
            }
            return new Blob([uInt8Array], {
                type: contentType
            });
        };
        let aLink = document.createElement('a');
        let blob = base64ToBlob(content); //new Blob([content]);
        let evt = document.createEvent("HTMLEvents");
        evt.initEvent("click", true, true); //initEvent 不加后两个参数在FF下会报错  事件类型，是否冒泡，是否阻止浏览器的默认行为
        aLink.download = fileName;
        aLink.href = URL.createObjectURL(blob);
        aLink.click();
    }

    /**
     * 将给定链接生成二维码同时下载
     * @param link
     * @param fileName
     */
    static downloadQrCodeWithLink(link, fileName) {
        let dom = document.createElement('canvas')

        QRCode.toCanvas(dom, link, (error, data) => {

            if (error) {
                console.error(error, '下载二维码图片异常')
            } else {
                Util.downloadImgByBase64(data.toDataURL(), fileName)
            }

        })
    }

    static downloadExcel(res, fileName) {
        let blob = res.data
        let reader = new FileReader()
        reader.readAsDataURL(blob)
        reader.onload = (e) => {
            let a = document.createElement('a')
            a.download = fileName+'.xlsx'
            a.href = e.target.result
            document.body.appendChild(a)
            a.click()
            document.body.removeChild(a)
        }
    }

    static uuid() {
        return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0,
                v = c == 'x' ? r : r & 0x3 | 0x8;
            return v.toString(16);
        });
    }

    static clone(obj) {

        if (!obj) {
            return obj;
        } else if (typeof obj === 'object') {
            let returnObj = null
            if (Array.isArray(obj)) {
                returnObj = []
                for (let i = 0; i < obj.length; i++) returnObj.push(obj[i])
            } else if (obj instanceof Date) {
                returnObj = new Date(obj.getTime())
            } else {
                returnObj = {}
                for (let key in obj) {
                    returnObj[key] = Util.clone(obj[key])
                }
            }
            return returnObj
        }
        return obj
    }

    /**
     * 用户组二维码地址
     * @param id
     * @returns {string}
     */
    static generateQrcodeUrlWithGroupId(id) {
        let url = `${window.location.protocol}//${window.location.host}/questionmobile/user/login?userGroupId=${id}`
        return url;
    }


}


/**
 * 日期格式化
 * @param fmt
 * @returns {*}
 */
Date.prototype.format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


Util.pageSize = 10;

Util.questionScoreArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
Util.questionScoreMultiArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]

Util.suffixArrayOfMusic = ["mp3", "wave"]

Util.suffixArrayOfPicture = ["jpg", "jpeg", "png"]

Util.backendUrl = location.href.indexOf("localhost") > -1 ? 'http://www.zhuancaiqian.com:8360' : 'http://' + location.hostname + ':8360'


export {Util}

import store from '../store'
import axios from 'axios'
// axios 配置
axios.defaults.timeout = 60000;
axios.defaults.baseURL = '';

let baseURL = `http://${location.host}:8360/`

if (location.href.indexOf("localhost") > -1) {
    baseURL = `http://127.0.0.1:8360/`
    // baseURL = `http://192.168.0.106:8360/`
}


// let baseURL = `http://${location.host}:8360/`


// if (location.host.indexOf('prep') > -1 || location.host.indexOf('localhost') > -1) {
//     baseURL = 'http://localhost:3000/'
// } else if (location.host.indexOf('open') > -1) {
//     baseURL = 'https://open.tingjiandan.com/'
// }


// http request 拦截器
axios.interceptors.request.use(
    config => {

        config.url = baseURL + config.url

        console.log(config.url)

        config.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

        config.headers.token = sessionStorage.getItem("token") || '';
        config.headers.userId = sessionStorage.getItem("userId") || '';

        return config;
    },
    err => {
        return Promise.reject(err);
    });
// http response 拦截器
axios.interceptors.response.use(
    response => {

        if (response.data.success === 2) {
            store.commit("reset")

            location.href = location.href.split("#")[0] + "#" + "/user/login"

            return Promise.reject(response.data.error);
        } else if (response.data.success === 1) {
            return Promise.reject(response.data.error);
        } else if (response.data.success === 0) {
            return Promise.resolve(response.data);
        }

    },
    error => {


        let errorMsg = "请求异常"
        if (error.message && error.message === 'Network Error') {
            errorMsg = "网络异常"
        } else if (error.response.status === 404) {
            errorMsg = "请求地址不存在"
        }


        return Promise.reject(errorMsg)

    }
);

export default axios;

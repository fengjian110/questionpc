import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import administratorList from "../pages/user/administratorList";
import administratorOperate from "../pages/user/administratorOperate";

Vue.use(VueRouter)



const homepage = r => require.ensure([], () => r(require('../pages/homepage')), 'homepage')


const user_login = r => require.ensure([], () => r(require('../pages/user/login')), 'user_login')
const user_register = r => require.ensure([], () => r(require('../pages/user/register')), 'user_register')
const user_detail = r => require.ensure([], () => r(require('../pages/user/detail')), 'user_detail')
const user_list = r => require.ensure([], () => r(require('../pages/user/list')), 'user_list')
const user_operate = r => require.ensure([], () => r(require('../pages/user/operate')), 'user_operate')

const user_updatePassword= r => require.ensure([], () => r(require('../pages/user/password')), 'user_updatePassword')


const user_administratorList = r => require.ensure([], () => r(require('../pages/user/administratorList')), 'user_administratorList')
const user_administratorOperate = r => require.ensure([], () => r(require('../pages/user/administratorOperate')), 'user_administratorOperate')



const userGroup_list = r => require.ensure([], () => r(require('../pages/userGroup/list')), 'userGroup_list')
const userGroup_operate = r => require.ensure([], () => r(require('../pages/userGroup/operate')), 'userGroup_operate')

const statistics_list = r => require.ensure([], () => r(require('../pages/statistics/list')), 'statistics_list')

const paper_list = r => require.ensure([], () => r(require('../pages/paper/list')), 'paper_list')
const paper_Query = r => require.ensure([], () => r(require('../pages/paper/listQuery')), 'paper_Query')
const paper_detail = r => require.ensure([], () => r(require('../pages/paper/detail')), 'paper_detail')
const basePaper = r => require.ensure([], () => r(require('../pages/paper/basePaper')), 'basePaper')

const measure_list = r => require.ensure([], () => r(require('../pages/measure/list')), 'measure_list')
const measure_listQuery = r => require.ensure([], () => r(require('../pages/measure/listQuery')), 'measure_listQuery')
const measure_detail = r => require.ensure([], () => r(require('../pages/measure/detail')), 'measure_detail')
const measure_operate = r => require.ensure([], () => r(require('../pages/measure/operate')), 'measure_operate')
const measure_group = r => require.ensure([], () => r(require('../pages/measure/group')), 'measure_group')
const measure_givenAnswerList = r => require.ensure([], () => r(require('../pages/measure/givenAnswerList')), 'givenAnswerList')
const baseMeasure = r => require.ensure([], () => r(require('../pages/measure/baseMeasure')), 'baseMeasure')

const settings_helpCenter = r => require.ensure([], () => r(require('../pages/settings/helpCenter')), 'settings_helpCenter')


const test_grid = r => require.ensure([], () => r(require('../pages/test/grid')), 'test_grid')
const test_flex = r => require.ensure([], () => r(require('../pages/test/flex')), 'test_flex')


const router=new VueRouter({
    base:'questionpc',
    // mode:'history',
    routes:[
        {
            path:'/',
            component:homepage
        },

        //user
        {
            path:'/user/list',
            component:user_list
        },
        {
            path:'/user/login',
            component:user_login
        },{
            path:'/user/register',
            component:user_register
        },{
            path:'/user/detail',
            component:user_detail
        },{
            path:'/user/operate',
            component:user_operate
        },,{
            path:'/user/updatePassword',
            component:user_updatePassword
        },{
            path:'/user/administratorList',
            component:user_administratorList
        },{
            path:'/user/administratorOperate',
            component:user_administratorOperate
        },

        //统计管理
        {
            path:'/statistics/list',
            component:statistics_list
        },

        //问卷管理
        {
            path:'/paper/list',
            component:paper_list
        },{
            path:'/paper/listQuery',
            component:paper_Query
        },{
            path:'/paper/detail',
            component:paper_detail
        },{
            path:'/paper/basePaper',
            component:basePaper
        },

        //量表管理
        {
            path:'/measure/list',
            component:measure_list
        },{
            path:'/measure/listQuery',
            component:measure_listQuery
        },{
            path:'/measure/detail',
            component:measure_detail
        },{
            path:'/measure/operate',
            component:measure_operate
        },{
            path:'/measure/group',
            component:measure_group
        },{
            path:'/measure/givenAnswerList',
            component:measure_givenAnswerList
        },{
            path:'/measure/baseMeasure',
            component:baseMeasure
        },

        //userGroup
        {
            path:'/userGroup/list',
            component:userGroup_list
        },{
            path:'/userGroup/operate',
            component:userGroup_operate
        },

        //settings
        {
            path:'/settings/helpCenter',
            component:settings_helpCenter
        },

        //test
        {
            path:'/test/grid',
            component:test_grid
        },{
            path:'/test/flex',
            component:test_flex
        },


    ]
})

router.afterEach((to,from,next)=>{




    //切换路由时菜单动态跟随切换
        store.commit('activeMenuName',to.meta.activeName)

})

export default router
